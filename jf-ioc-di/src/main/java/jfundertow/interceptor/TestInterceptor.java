package jfundertow.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

import java.lang.reflect.Method;

public class TestInterceptor implements Interceptor {
    @Override
    public void intercept(Invocation inv) {
        System.out.println("TestInterceptor");


        Object target = inv.getTarget();
        Method method = inv.getMethod();
        Object[] args = inv.getArgs();

        //
        inv.invoke();
    }
}
