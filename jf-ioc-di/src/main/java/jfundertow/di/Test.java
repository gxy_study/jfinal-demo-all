package jfundertow.di;

import com.jfinal.aop.Aop;

public class Test {
    public static void main(String[] args) {
        B b = Aop.get(B.class);
        b.a.b.say_hello();
    }
}
