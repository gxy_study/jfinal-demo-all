package jfundertow.di;

import com.jfinal.aop.Inject;
import com.jfinal.aop.Singleton;

@Singleton(true)
public class A {
    @Inject
    public B b;
}
