package jfundertow.ioc;

import com.jfinal.aop.Aop;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.InterceptorManager;
import com.jfinal.aop.Invocation;
import jfundertow.di.B;

public class Test {
    public static void main(String[] args) {
        InterceptorManager.me().addGlobalServiceInterceptor(new Interceptor() {
            @Override
            public void intercept(Invocation inv) {
                System.out.println(this.getClass().getTypeName());
                //
                inv.invoke();
            }
        });

        B b = Aop.get(B.class);
        b.a.b.say_hello();
    }
}
