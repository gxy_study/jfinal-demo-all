package jfmultiwebapps.core;

import com.jfinal.config.JFinalConfig;

public abstract class WebappConfig extends JFinalConfig {
    public String contex_path = "/";


    public WebappConfig(String contex_path) {
        this.contex_path = contex_path;
    }
}
