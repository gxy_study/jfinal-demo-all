package jfmultiwebapps.webapp1;

import com.jfinal.config.*;
import com.jfinal.core.Controller;
import com.jfinal.template.Engine;
import jfmultiwebapps.core.WebappConfig;

public class WebappConfig1 extends WebappConfig {
    public WebappConfig1(String contex_path) {
        super(contex_path);
    }

    @Override
    public void configConstant(Constants me) {

    }

    @Override
    public void configRoute(Routes me) {
        configRoute(me, "", Webapp1Controller.class);
    }

    public void configRoute(Routes me, String controllerKey, Class<? extends Controller> controllerClass) {
        me.add(contex_path + controllerKey, controllerClass);
    }

    @Override
    public void configEngine(Engine me) {

    }

    @Override
    public void configPlugin(Plugins me) {

    }

    @Override
    public void configInterceptor(Interceptors me) {

    }

    @Override
    public void configHandler(Handlers me) {

    }
}
