package jfmultitplengine;

import com.jfinal.server.undertow.UndertowConfig;
import com.jfinal.server.undertow.UndertowServer;
import io.undertow.jsp.HackInstanceManager;
import io.undertow.jsp.JspServletBuilder;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.servlet.api.DeploymentInfo;
import jfmultitplengine.jsp.TldLocator;
import org.apache.jasper.Constants;
import org.apache.jasper.deploy.TagLibraryInfo;
import org.apache.tomcat.InstanceManager;

import java.util.HashMap;
import java.util.Map;

public class MyUndertowServer extends UndertowServer {
    protected MyUndertowServer(UndertowConfig undertowConfig) {
        super(undertowConfig);
    }


    @Override
    protected void configUndertow() {
        super.configUndertow();
        configJsp(deploymentInfo);
    }


    /**
     * undertow jsp配置
     *
     * @author 张天笑
     * @time 2018/12/28 22:28
     */
    public void configJsp(DeploymentInfo di) {
        di.addServlet(JspServletBuilder.createServlet("jsp_servlet", "*.jsp"));

        Map<String, TagLibraryInfo> tags = buildJspTagLibInfo();
        di.addServletContextAttribute(Constants.JSP_TAG_LIBRARIES, tags);

        di.addServletContextAttribute(InstanceManager.class.getName(), new HackInstanceManager());

        di.setResourceManager(new ClassPathResourceManager(MyUndertowServer.class.getClassLoader()));
    }

    public HashMap<String, TagLibraryInfo> buildJspTagLibInfo() {
        try {
            return TldLocator.createTldInfos();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }
}
