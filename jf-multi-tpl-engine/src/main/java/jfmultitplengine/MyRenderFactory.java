package jfmultitplengine;

import com.jfinal.render.Render;
import com.jfinal.render.RenderFactory;
import com.jfinal.template.source.ClassPathSourceFactory;
import jfmultitplengine.thymeleaf.ThymeleafRender;

public class MyRenderFactory extends RenderFactory {

    @Override
    public Render getRender(String view) {


        Render render = null;
        if (view.endsWith(".jsp")) {
            render = super.getJspRender(view);
        } else if (view.endsWith(".ftl")) {
            render = super.getFreeMarkerRender(view);
        } else if (view.endsWith(".thym")) {
            render = new ThymeleafRender("/", view, new ClassPathSourceFactory());
        } else {
            render = super.getDefaultRender(view);
        }
        return render;
    }
}
