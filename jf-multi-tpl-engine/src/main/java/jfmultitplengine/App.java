package jfmultitplengine;

import com.jfinal.server.undertow.UndertowConfig;

public class App {
    public static void main(String[] args) {
        new MyUndertowServer(new UndertowConfig(DemoConfig.class)).start();
    }

}
