<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>jsp</title>
</head>
<body>
<%
    out.println("Your IP address is " + request.getRemoteAddr());
%>
<br/>=============<br/>
<h1><%= request.getAttribute("title") %>
</h1>
<br/>=============<br/>
<h1>${title}</h1>
<br/>=============<br/>
<h1><c:forEach var="a" items="${arr}">${a}</c:forEach></h1>
</body>
</html>