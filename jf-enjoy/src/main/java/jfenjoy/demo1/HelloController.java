package jfenjoy.demo1;

import com.jfinal.core.Controller;

public class HelloController extends Controller {
    public void index() {
        setAttr("msg","你好, 我叫张天笑");
        render("/index.html");
    }

    /*
    public void hot() {
        renderText("Hot!!!");
    }
    */
}