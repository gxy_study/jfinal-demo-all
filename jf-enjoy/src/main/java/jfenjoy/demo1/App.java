package jfenjoy.demo1;

import com.jfinal.server.undertow.UndertowServer;

public class App {
    public static void main(String[] args) {
        /*
         * 最简洁的启动方式, 全部默认配置
         * undertow.txt
         * */
        UndertowServer.start(DemoConfig.class);

        //curl http://localhost:8080/hello

        //打开hello controller中hot方法的注释, ctrl+f9编译
        //curl http://localhost:8080/hello/hot
    }

}
