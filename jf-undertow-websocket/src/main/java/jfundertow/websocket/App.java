package jfundertow.websocket;

import com.jfinal.server.undertow.UndertowConfig;
import io.undertow.Undertow;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.spi.WebSocketHttpExchange;

import static io.undertow.Handlers.path;
import static io.undertow.Handlers.resource;
import static io.undertow.Handlers.websocket;

public class App {
    public static void main(final String[] args) {
        new MyServer(new UndertowConfig(DemoConfig.class)).start();

        // 打开chrome浏览器访问 http://localhost:8080/hello

    }
}
