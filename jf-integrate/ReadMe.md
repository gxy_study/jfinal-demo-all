
### Groovy脚本启动和脚本停止

##### 使用须知
0. 依赖groovy环境
1. 此demo不需要打jar包
2. 此demo依赖jar和源码是分开的, 使用前需要将源代码, 资源文件编译好(这一步在开发阶段已经做好了),
使用maven-dependencies插件提取依赖jar

##### 使用说明 
0. 进入项目根目录执行 `mvn install`
1. 下载groovy的zip版本, 不要用installer.exe, 添加bin路径到环境变量
2. 进入jf-integrate根目录, 根据注释修改几个变量, 
3. 尝试执行`groovy start.groovy`,`groovy stop.groovy` 