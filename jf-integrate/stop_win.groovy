/*
 需要改的只有项目占用的端口
 */
def port = "8080"

def process = Runtime.getRuntime().exec("netstat -ano")  // 加上搜索管道符号会直接返回空字符
def fis = process.inputStream

BufferedReader br = new BufferedReader(new InputStreamReader(fis));
def list = new ArrayList<String>()
while ((line = br.readLine()) != null) {
    list.add(line)
}

for (int i = 0; i < list.size(); i++) {
    if (i > 2) {
        def it = list.get(i)
        def split = it.split("\\s+")
        def address = split[2]
        if (split.length > 2 && address == "0.0.0.0:" + port) {
            def pid = split[5]
            println it
            def command = "taskkill /pid ${pid} /F"
            Runtime.getRuntime().exec(command)
        }
    }
}