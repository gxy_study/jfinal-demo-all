/**
 * create by zhang_tian_xiao, 2018/8/7 10:32
 *
 * */
package com.ax.framework.jfinal.db

import com.alibaba.fastjson.serializer.JSONSerializer
import com.alibaba.fastjson.serializer.ObjectSerializer
import com.jfinal.plugin.activerecord.CPI
import com.jfinal.plugin.activerecord.Model
import java.io.IOException
import java.lang.reflect.Type

class JFinalModelSerializer : ObjectSerializer {
    @Throws(IOException::class)
    override fun write(serializer: JSONSerializer, `object`: Any?, fieldName: Any?, fieldType: Type?, features: Int) {
        if (`object` != null) {
            val map = CPI.getAttrs((`object` as Model<*>?)!!)
            serializer.write(map)
        }
    }
}
