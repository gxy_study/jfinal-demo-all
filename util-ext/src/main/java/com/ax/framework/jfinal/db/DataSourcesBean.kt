package com.ax.framework.jfinal.db

import com.alibaba.fastjson.JSONObject
import java.util.ArrayList

/**
 *@author  小杨
 *@datetime  2018/11/22 0022 14:27
 *
神奇, 泛型加在类上Yaml不给解析,
class DataSourcesBean : ArrayList<JSONObject>
val data_sources_bean = Yaml().loadAs(stream, DataSourcesBean::class.java)\
get出来是LinkHashMap

 * */
class DataSourcesBean {
    //写成属性, 就可以解析到JSONObject, 而不是LinkedHashMap
    private lateinit var _datasource: ArrayList<JSONObject>

    fun setDatasource(a: ArrayList<JSONObject>) {
        this._datasource = a;
    }

    fun getValue() = this._datasource
}