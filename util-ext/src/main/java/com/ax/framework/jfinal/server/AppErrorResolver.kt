package com.ax.framework.jfinal.server

import cn.hutool.core.util.ReflectUtil
import cn.hutool.log.LogFactory
import com.jfinal.core.Action
import com.jfinal.render.RenderManager
import java.lang.Exception
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

val log = LogFactory.get()

fun markdownAppErrorResolver(req: HttpServletRequest, resp: HttpServletResponse, action: Action, e: Exception) {
    //val e = Exception("\n${req.requestURI}  ${e.message}", e)
    println(req.requestURI)
    log.error(e)


    val map = hashMapOf<String, String>()

    map.set("a", "b")

    RenderManager.me().getRenderFactory().getJsonRender(map).setContext(req, resp).render()

}