package com.ax


/*
 * 拿到数组中元素的类型
 * */
fun Array<*>.getTypeOfArray(): String {
    return this::class.javaObjectType.simpleName.replace("[]", "")
}
