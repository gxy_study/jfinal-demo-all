package com.ax

import cn.hutool.core.util.StrUtil
import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.serializer.NameFilter

/*
* 强制类型转换, 少写个括号
* */
fun <T> Any.asInstanceOf() = this as T

/**
转为下划线格式的json
 * */
fun <T> T.toUnderLineJson(): String {
    return JSON.toJSONString(this, NameFilter { _, propertyName, _ ->
        StrUtil.toUnderlineCase(propertyName)
    })
}


/*
* 转为驼峰格式的json
* */
fun <T> T.toHumpJson(): String {
    return JSON.toJSONString(this)
}