# jfinal-demo-all

#### Description
#### 这是一个演示JFinal 各种用法的demo, 纯Java编写

1. ioc, di, 
2. web场景下的ioc, di  
3. 方法, 类级别 热加载  
4. websocket , onText, onBinary(jf-undertow-websocket/)
5. kotlin下使用DbRecord的最佳姿势! (jf-arp/)
6. win下的一键启动, 关闭的groovy脚本(jf-integrate/) 
7. jf_enjoy
8. JFinal-MVC 多视图支持示例, jsp,thymeleaf,freemarker()(jf-multi-tpl-engine/)
9. 假装像标准web容器一样, 部署多个应用(jf-multi-webapps/)
10. ...